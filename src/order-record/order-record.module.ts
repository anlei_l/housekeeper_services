import { Module } from '@nestjs/common';
import { OrderRecordService } from './order-record.service';
import { OrderRecordController } from './order-record.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderRecord } from './entities/order-record.entity';

@Module({
  imports: [TypeOrmModule.forFeature([OrderRecord])],
  controllers: [OrderRecordController],
  providers: [OrderRecordService],
  exports: [OrderRecordService]
})
export class OrderRecordModule {}

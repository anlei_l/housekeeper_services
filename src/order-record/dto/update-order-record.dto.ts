import { PartialType } from '@nestjs/mapped-types';
import { CreateOrderRecordDto } from './create-order-record.dto';

export class UpdateOrderRecordDto extends PartialType(CreateOrderRecordDto) {}

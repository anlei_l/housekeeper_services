import { Order } from '../../order/order.entity';

export class CreateOrderRecordDto {
  readonly status: number;
  readonly order: Order;
}

import { Injectable } from '@nestjs/common';
import { CreateOrderRecordDto } from './dto/create-order-record.dto';
import { UpdateOrderRecordDto } from './dto/update-order-record.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { OrderRecord } from './entities/order-record.entity';
import { Repository } from 'typeorm';

@Injectable()
export class OrderRecordService {
  constructor(
    @InjectRepository(OrderRecord)
    private orderRecordRepository: Repository<OrderRecord>,
  ) {}

  async create(createOrderRecordDto: CreateOrderRecordDto) {
    const orderRecordModal =
      this.orderRecordRepository.create(createOrderRecordDto);
    return await this.orderRecordRepository.save(orderRecordModal);
  }

  findAll() {
    return `This action returns all orderRecord`;
  }

  findOne(id: number) {
    return `This action returns a #${id} orderRecord`;
  }

  update(id: number, updateOrderRecordDto: UpdateOrderRecordDto) {
    return `This action updates a #${id} orderRecord`;
  }

  remove(id: number) {
    return `This action removes a #${id} orderRecord`;
  }
}

import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';
import { Order } from '../../order/order.entity';

export enum OrderStatus {
  '已取消',
  '新订单',
  '待检测',
  '待服务',
  '已完成',
}

@Entity()
export class OrderRecord {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'enum', enum: OrderStatus, default: 1 })
  status: OrderStatus;

  @ManyToOne((type) => Order, (order) => order.records)
  order: Order;

  @CreateDateColumn()
  createAt: string;

  @UpdateDateColumn()
  updateAt: string;

  @VersionColumn()
  version: string;
}

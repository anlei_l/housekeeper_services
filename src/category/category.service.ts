import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Category } from './category.entity';

@Injectable()
export class CategoryService {
  constructor(
    @InjectRepository(Category)
    private categoryRepository: Repository<Category>,
  ) {}

  findAll(): Promise<Category[]> {
    return this.categoryRepository.find({ relations: ["projects"]  });
  }

  async findOne(id): Promise<Category> {
    return await this.categoryRepository.findOne(id);
  }

  async create(body: Category): Promise<any> {
    const user = this.categoryRepository.create(body);
    const result = await this.categoryRepository.save(user);
    return result;
  }

  async remove(id: string): Promise<void> {
    this.categoryRepository.delete(id);
  }
}

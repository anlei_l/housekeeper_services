import {
  Bind,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post, UseGuards,
} from '@nestjs/common';
import { CategoryService } from './category.service';
import { Category } from './category.entity';
import {JwtAuthGuard} from "../auth/jwt-auth.guard";

@Controller('category')
export class CategoryController {
  constructor(private categoryService: CategoryService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  async findAll(): Promise<Category[]> {
    return await this.categoryService.findAll();
  }

  @Get(':id')
  @Bind(Param())
  async findOne(params): Promise<Category> {
    return await this.categoryService.findOne(params.id);
  }

  @Post()
  async add(@Body() body: Category): Promise<any> {
    return await this.categoryService.create(body);
  }

  @Delete(':id')
  async remove(@Param() params): Promise<void> {
    return await this.categoryService.remove(params.id);
  }
}

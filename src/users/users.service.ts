import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Users } from './Users.entity';
import { Connection, Repository } from 'typeorm';
import axios from 'axios';
import WXBizDataCrypt from './WXBizDataCrypt';
import { Project } from '../project/project.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Users)
    private usersRepository: Repository<Users>,
    @InjectConnection()
    private connection: Connection,
  ) {}

  findAll(condition?: any): Promise<Users[]> {
    return this.usersRepository.find({ ...condition });
  }

  async getFreeEngineer(role, id) {
    return await this.connection
      .createQueryBuilder()
      .select('users')
      .from('users', 'users')
      .leftJoinAndSelect(Project, 'project', 'users.project = project.id')
      .where('users.role = :role', { role })
      .andWhere('users.projectId = :id', { id })
      .getMany();
  }

  async findOne(user: any): Promise<Users> {
    return await this.usersRepository.findOne({ ...user });
  }

  async create(body: Users): Promise<any> {
    const user = this.usersRepository.create(body);
    const result = await this.usersRepository.save(user);
    return result;
  }

  async remove(id: string): Promise<void> {
    this.usersRepository.delete(id);
  }

  async update(phoneNumber): Promise<Users> {
    const user = await this.usersRepository.findOne({ phoneNumber })
    user.isReceiving = !user.isReceiving
    return await this.usersRepository.save(user)
  }

  session_key = '';

  async getUserInfo({ code, phoneInfo, userInfo }) {
    try {
      const query = {
        appid: 'wxa38c7d8201634bb9',
        secret: '2f93bf215dbb4563f88b31537a3a3cb3',
        js_code: code,
        grant_type: 'authorization_code',
      };
      const queryString = Object.keys(query)
        .map((item) => `${item}=${query[item]}`)
        .join('&');
      if (!this.session_key) {
        const { data } = await axios.get(
          `https://api.weixin.qq.com/sns/jscode2session?${queryString}`,
        );
        this.session_key = data?.session_key;
      }
      const pc = new WXBizDataCrypt(query.appid, this.session_key);
      const phone: any = pc.decryptData(phoneInfo.encryptedData, phoneInfo.iv);
      const user: any = pc.decryptData(userInfo.encryptedData, userInfo.iv);
      const info = {
        ...JSON.parse(phone),
        ...JSON.parse(user),
        appid: JSON.parse(user)?.watermark?.appid,
      };
      delete info.watermark;
      return info;
    } catch (e) {
      console.log(e);
      return e;
    }
  }
}

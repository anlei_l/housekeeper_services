import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';
import { Order } from '../order/order.entity';
import { Address } from '../address/address.entity';
import { Project } from '../project/project.entity';

@Entity()
export class Users {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: '' })
  phoneNumber: string;

  @Column({ default: true })
  isActive: boolean;

  @Column({ default: '' })
  openId: string;

  @Column({ default: '' })
  nickName: string;

  @Column({ default: 0 })
  gender: number;

  @Column({ default: '' })
  language: string;

  @Column({ default: '' })
  avatarUrl: string;

  @Column({ default: 0 })
  timestamp: number;

  @Column({ default: '' })
  appid: string;

  @Column({ default: false })
  isReceiving: boolean;

  @Column({ default: 1 })
  role: number; //1普通用户 2工程师

  @Column({ default: '' })
  city: string;

  @Column({ default: '' })
  province: string;

  @Column({ default: '' })
  country: string;

  @ManyToOne((type) => Project, (project) => project.id)
  project: Project;

  @Column({ default: '' })
  purePhoneNumber: string;

  @Column({ default: '' })
  countryCode: string;

  @OneToMany((type) => Order, (order) => order.id)
  orders: Order[];

  @OneToMany((type) => Address, (address) => address.id)
  address: Address;

  @CreateDateColumn()
  createAt: string;

  @UpdateDateColumn()
  updateAt: string;

  @VersionColumn()
  version: string;
}

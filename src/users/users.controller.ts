import {
  Bind,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Request,
  Post,
  UseGuards,
  Put,
} from '@nestjs/common';
import { Users } from './users.entity';
import { UsersService } from './users.service';
import { AuthService } from '../auth/auth.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

@Controller('user')
export class UsersController {
  constructor(
    private userService: UsersService,
    private authService: AuthService,
  ) {}

  @Get()
  async findAll(): Promise<Users[]> {
    return await this.userService.findAll();
  }

  @Post()
  async add(@Body() body: Users): Promise<any> {
    return await this.userService.create(body);
  }

  @Delete(':id')
  async remove(@Param() params): Promise<void> {
    return await this.userService.remove(params.id);
  }

  // @Post('sign')
  // @Bind(Body())
  // async sign(body) {
  //   return this.userService.create(body);
  // }

  @Post('/login')
  @Bind(Body())
  async login(body: any): Promise<any> {
    try {
      let response = {
        access_token: undefined,
        avatarUrl: undefined,
        id: undefined,
        nickName: undefined,
        isReceiving: undefined,
        role: undefined,
      };
      const userInfo: any = await this.userService.getUserInfo(body);
      const authResult = await this.authService.validateUser(userInfo);
      if (authResult) {
        const auth = await this.authService.getToken(authResult)
        response.access_token = auth.access_token
        response.id = authResult.id;
        response.nickName = authResult.nickName;
        response.avatarUrl = authResult.avatarUrl;
        response.isReceiving = authResult.isReceiving;
        response.role = authResult.role;
        return response;
      }
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get('profile')
  @Bind(Request())
  async getProfile(req) {
    const query = { phoneNumber: req.user.phoneNumber };
    return await this.userService.findOne(query);
  }

  @UseGuards(JwtAuthGuard)
  @Put('startOrStopReceiving')
  @Bind(Request())
  async startOrStopReceivingOrder(req) {
    return await this.userService.update(req.user.phoneNumber);
  }
}

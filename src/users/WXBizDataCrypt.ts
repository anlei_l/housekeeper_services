import * as crypto from 'crypto';

class WXBizDataCrypt {
  private appId: string;
  private sessionKey: string;

  constructor(appId, sessionKey) {
    this.appId = appId;
    this.sessionKey = sessionKey;
  }

  decryptData(encryptedData, iv) {
    const sessionKey = new Buffer(this.sessionKey, 'base64');
    encryptedData = new Buffer(encryptedData, 'base64');
    iv = new Buffer(iv, 'base64');

    try {
      // 解密
      const decipher = crypto.createDecipheriv('aes-128-cbc', sessionKey, iv);
      decipher.setAutoPadding(true);
      let decoded = decipher.update(encryptedData, 'binary', 'utf8');
      decoded += decipher.final('utf8');

      if (JSON.parse(decoded).watermark.appid !== this.appId) {
        throw new Error('Illegal Buffer');
      }

      return decoded;
    } catch (err) {
      throw new Error(err);
    }
  }
}

export default WXBizDataCrypt;

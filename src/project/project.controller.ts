import {
  Bind,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
} from '@nestjs/common';
import { ProjectService } from './project.service';
import { Project } from './project.entity';

@Controller('project')
export class ProjectController {
  constructor(private projectService: ProjectService) {}

  @Get()
  @Bind(Query())
  async findAll(query): Promise<Project[]> {
    return await this.projectService.findAll(query);
  }

  @Get(':id')
  @Bind(Param())
  async findOne(params): Promise<Project> {
    return await this.projectService.findOne(params.id);
  }

  @Post()
  async add(@Body() body: Project): Promise<any> {
    return await this.projectService.create(body);
  }

  @Delete(':id')
  async remove(@Param() params): Promise<void> {
    return await this.projectService.remove(params.id);
  }
}

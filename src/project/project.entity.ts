import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';
import { Category } from '../category/category.entity';
import { Users } from '../users/users.entity';
import { Order } from '../order/order.entity';
import { OrderRecord } from '../order-record/entities/order-record.entity';

@Entity()
export class Project {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: '' })
  name: string;

  @Column({ default: '' })
  thumbnail: string;

  @Column({ default: '' })
  banner: string;

  @Column({ default: '0.00', type: 'decimal' })
  price: string;

  @Column({ default: true })
  createUser: boolean;

  @ManyToOne((type) => Category, (category) => category.projects)
  category: Category;

  @OneToMany((type) => Order, (order) => order.id)
  orders: Order[];

  @OneToMany((type) => Users, (users) => users.project)
  users: Users[];

  @CreateDateColumn()
  createAt: string;

  @UpdateDateColumn()
  updateAt: string;

  @VersionColumn()
  version: string;
}

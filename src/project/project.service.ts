import { Dependencies, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Project } from './project.entity';
import { CategoryService } from '../category/category.service';

@Injectable()
@Dependencies(CategoryService)
export class ProjectService {
  private categoryService: CategoryService;

  constructor(
    categoryService,
    @InjectRepository(Project)
    private projectRepository: Repository<Project>,
  ) {
    this.categoryService = categoryService;
  }

  findAll(query: any): Promise<Project[]> {
    return this.projectRepository.find({ ...query, relations: ['category'] });
  }

  async findOneWhereCategory(id): Promise<Project> {
    return await this.projectRepository.findOne(
      { category: id },
      { relations: ['category'] },
    );
  }

  async findOne(id): Promise<Project> {
    return await this.projectRepository.findOne(id, {
      relations: ['category'],
    });
  }

  async getUsers(id): Promise<Project> {
    return await this.projectRepository.findOne(id, {
      relations: ['users'],
    });
  }

  async create(body: Project): Promise<any> {
    const entity = this.projectRepository.create(body);
    const result = await this.projectRepository.save(entity);
    return result;
  }

  async remove(id: string): Promise<void> {
    this.projectRepository.delete(id);
  }
}

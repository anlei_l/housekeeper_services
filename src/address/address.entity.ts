import {
  Column,
  CreateDateColumn,
  Entity, ManyToOne, OneToMany,
  PrimaryGeneratedColumn, UpdateDateColumn, VersionColumn,
} from 'typeorm';
import {Category} from "../category/category.entity";
import {Users} from "../users/users.entity";
import {Order} from "../order/order.entity";

@Entity()
export class Address {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: '' })
  concatName: string;

  @Column({ default: '' })
  phoneNo: string;

  @Column({ default: '' })
  province: string

  @Column({ default: '' })
  city: string

  @Column({ default: '' })
  area: string

  @Column({ default: '' })
  address: string

  @Column({ default: false })
  isDefault: boolean

  @ManyToOne(type => Users, users => users.id)
  createUser: string

  @OneToMany(type => Order, order => order.id)
  orders: Order[]

  @CreateDateColumn()
  createAt: string;

  @UpdateDateColumn()
  updateAt: string;

  @VersionColumn()
  version: string;
}

import { Dependencies, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Address } from './address.entity';

@Injectable()
export class AddressService {
  constructor(
    @InjectRepository(Address)
    private addresspository: Repository<Address>,
  ) {}

  findAll(query: any): Promise<Address[]> {
    return this.addresspository.find();
  }

  async findOne(id): Promise<Address> {
    return await this.addresspository.findOne(id);
  }

  async create(body: Address): Promise<any> {
    const entity = this.addresspository.create(body);
    const result = await this.addresspository.save(entity);
    return result;
  }

  async update(body: Address): Promise<any> {
    if (body.isDefault) {
      const defaultAddress = await this.addresspository.findOne({ isDefault: true })
      await this.addresspository.save({
        ...defaultAddress,
        isDefault: false
      })
    }
    const address = await this.addresspository.findOne(body.id);
    return await this.addresspository.save({
      ...address,
      ...body
    })
  }

  async remove(id: string): Promise<void> {
    this.addresspository.delete(id);
  }
}

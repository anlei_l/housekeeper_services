import {
  Bind,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query, UseGuards,
  Request, Put
} from '@nestjs/common';
import { AddressService } from './address.service';
import { Address } from './address.entity';
import {JwtAuthGuard} from "../auth/jwt-auth.guard";

@Controller('address')
export class AddressController {
  constructor(private addressService: AddressService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  @Bind(Request(), Query())
  async findAll(req, query): Promise<any> {
    return await this.addressService.findAll({
      user: req.user.id,
      ...query
    });
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  @Bind(Param())
  async findOne(params): Promise<Address> {
    return await this.addressService.findOne(params.id);
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  @Bind(Request(), Body())
  async add(req, body: Address): Promise<any> {
    body.createUser = req.user.id
    return await this.addressService.create(body);
  }

  @UseGuards(JwtAuthGuard)
  @Put()
  @Bind(Request(), Body())
  async update(req, body: Address): Promise<any> {
    return await this.addressService.update(body);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async remove(@Param() params): Promise<void> {
    return await this.addressService.remove(params.id);
  }
}

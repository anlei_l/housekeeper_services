import { NestFactory } from '@nestjs/core';
import { ApplicationModule } from './Application.module';
import { join } from 'path';
import { NestExpressApplication } from '@nestjs/platform-express';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(
    ApplicationModule,
  );
  app.useStaticAssets('upload', {
    prefix: '/upload/',
  });
  await app.listen(3000);
}
bootstrap();

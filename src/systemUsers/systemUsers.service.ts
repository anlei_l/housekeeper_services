import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SystemUsers } from './systemUsers.entity';
import { Repository } from 'typeorm';

@Injectable()
export class SystemUsersService {
  constructor(
    @InjectRepository(SystemUsers)
    private usersRepository: Repository<SystemUsers>,
  ) {}

  findAll(): Promise<SystemUsers[]> {
    return this.usersRepository.find();
  }

  findOne(username: string): Promise<SystemUsers> {
    return this.usersRepository.findOne({ username });
  }

  async create(body: SystemUsers): Promise<any> {
    const user = this.usersRepository.create(body);
    const result = await this.usersRepository.save(user);
    return result;
  }

  async remove(id: string): Promise<void> {
    this.usersRepository.delete(id);
  }
}

import { forwardRef, Module } from '@nestjs/common';
import { SystemUsersService } from './systemUsers.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SystemUsers } from './systemUsers.entity';
import { SystemUsersController } from './systemUsers.controller';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([SystemUsers]),
    forwardRef(() => AuthModule),
  ],
  providers: [SystemUsersService],
  controllers: [SystemUsersController],
  exports: [SystemUsersService],
})
export class SystemUsersModule {}

import {
  Bind,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Request,
  Post,
  UseGuards,
  Dependencies,
} from '@nestjs/common';
import { SystemUsers } from './systemUsers.entity';
import { SystemUsersService } from './systemUsers.service';
import { LocalAuthGuard } from '../auth/local-auth.guard';
import { AuthService } from '../auth/auth.service';

@Controller('system/user')
export class SystemUsersController {
  constructor(
    private systemUserService: SystemUsersService,
    private authService: AuthService,
  ) {}

  @Get()
  async findAll(): Promise<SystemUsers[]> {
    return await this.systemUserService.findAll();
  }

  @Post('sign')
  async add(@Body() body: SystemUsers): Promise<any> {
    console.log(body);
    return await this.systemUserService.create(body);
  }

  @UseGuards(LocalAuthGuard)
  @Post('/login')
  @Bind(Request())
  async systemLogin(req) {
    return this.authService.systemLogin(req.user);
  }

  @Delete(':id')
  async remove(@Param() params): Promise<void> {
    return await this.systemUserService.remove(params.id);
  }
}

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { SystemUsersModule } from './systemUsers/systemUsers.module';
import { CategoryModule } from './category/category.module';
import { ProjectModule } from './project/project.module';
import { AddressModule } from './address/address.module';
import { UploadModule } from './upload/upload.module';
import { OrderModule } from './order/order.module';
import { OrderRecordModule } from './order-record/order-record.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '123456',
      database: 'housekeeper',
      autoLoadEntities: true,
      synchronize: true,
    }),
    AuthModule,
    UsersModule,
    SystemUsersModule,
    CategoryModule,
    ProjectModule,
    AddressModule,
    UploadModule,
    OrderModule,
    OrderRecordModule,
  ],
  controllers: [],
  providers: [],
})
export class ApplicationModule {}

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from './order.entity';
import { OrderService } from './order.service';
import { OrderController } from './order.controller';
import { UploadModule } from '../upload/upload.module';
import { UsersModule } from '../users/users.module';
import { ProjectModule } from '../project/project.module';
import {OrderRecordModule} from "../order-record/order-record.module";

@Module({
  imports: [
    TypeOrmModule.forFeature([Order]),
    UploadModule,
    UsersModule,
    ProjectModule,
    OrderRecordModule,
  ],
  providers: [OrderService],
  controllers: [OrderController],
  exports: [OrderService],
})
export class OrderModule {}

import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';

import { Users } from '../users/users.entity';
import { Address } from '../address/address.entity';
import { Project } from '../project/project.entity';
import { OrderRecord } from '../order-record/entities/order-record.entity';

export enum OrderStatus {
  '已取消',
  '新订单',
  '待检测',
  '待服务',
  '已完成',
}

@Entity()
export class Order {
  @PrimaryGeneratedColumn('uuid')
  id: number;

  @Column({ type: 'enum', enum: OrderStatus, default: 1 })
  status: OrderStatus;

  @ManyToOne((type) => Project, (project) => project.orders, {
    onDelete: 'CASCADE',
  })
  project: Project;

  @Column({ default: '' })
  images: string;

  @Column({ type: 'decimal', default: '0.00' })
  price: string;

  @ManyToOne((type) => Users, (users) => users.orders, {
    onDelete: 'CASCADE',
  })
  engineer: Users;

  @ManyToOne((type) => Address, (address) => address.orders)
  address: Address;

  @ManyToOne((type) => Users, (users) => users.orders, {
    onDelete: 'CASCADE',
  })
  createUser: Users;

  @OneToMany((type) => OrderRecord, (orderRecord) => orderRecord.order, {
    onDelete: 'CASCADE',
  })
  records: OrderRecord[];

  @CreateDateColumn()
  createAt: string;

  @UpdateDateColumn()
  updateAt: string;

  @VersionColumn()
  version: string;
}

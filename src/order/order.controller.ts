import {
  Bind,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post, UseGuards,
  Request, Req, Put
} from '@nestjs/common';
import { OrderService } from './order.service';
import { Order } from './order.entity';
import {JwtAuthGuard} from "../auth/jwt-auth.guard";

@Controller('order')
export class OrderController {
  constructor(
    private orderService: OrderService
  ) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  @Bind(Request())
  async findAll(req): Promise<Order[]> {
    return await this.orderService.findAll(req.user);
  }

  @Get(':id')
  @Bind(Param())
  async findOne(params): Promise<Order> {
    return await this.orderService.findOne(params.id);
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  @Bind(Request(), Body())
  async add(req, body: Order): Promise<any> {
    return await this.orderService.create({
      ...body,
      createUser: req.user
    });
  }

  @Delete(':id')
  async remove(@Param() params): Promise<void> {
    return await this.orderService.remove(params.id);
  }

  @Put('/cancelOrder/:id')
  @Bind(Param())
  async cancelOrder(param) {
    return this.orderService.cancellationOrder(param.id)
  }
}

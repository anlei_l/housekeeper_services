import { Dependencies, Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, createConnection, Repository } from 'typeorm';
import { Order } from './order.entity';
import { UploadService } from '../upload/upload.service';
import { UsersService } from '../users/users.service';
import { ProjectService } from '../project/project.service';
import { OrderRecordService } from '../order-record/order-record.service';

@Injectable()
@Dependencies(UploadService, UsersService, ProjectService, OrderRecordService)
export class OrderService {
  private uploadService: UploadService;
  private usersService: UsersService;
  private projectService: ProjectService;
  private orderRecordService: OrderRecordService;
  constructor(
    uploadService,
    usersService,
    projectService,
    orderRecordService,
    @InjectRepository(Order)
    private orderRepository: Repository<Order>,
  ) {
    this.uploadService = uploadService;
    this.usersService = usersService;
    this.projectService = projectService;
    this.orderRecordService = orderRecordService;
  }

  findAll(user): Promise<Order[]> {
    return this.orderRepository.find({
      relations: ['project', 'engineer', 'address', 'createUser'],
      where: {
        createUser: user.id
      }
    });
  }

  async findOne(id): Promise<Order> {
    return await this.orderRepository.findOne(id, {
      relations: ['project', 'engineer', 'address', 'createUser', 'records'],
    });
  }

  async create(body: Order): Promise<any> {
    const engeers: any[] = await this.usersService.getFreeEngineer(
      2,
      body.project.id,
    );

    if (engeers.length) {
      body.engineer = engeers[0];
    }

    const orderModel = this.orderRepository.create(body);
    const order = await this.orderRepository.save(orderModel);
    await this.orderRecordService.create({
      status: order.status,
      order,
    });
    return order;
  }

  async cancellationOrder(id) {
    const order = await this.orderRepository.findOne(id);
    order.status = 0;
    this.orderRecordService.create({
      status: 0,
      order,
    });
    return await this.orderRepository.save(order);
  }

  async remove(id: string): Promise<void> {
    this.orderRepository.delete(id);
  }
}

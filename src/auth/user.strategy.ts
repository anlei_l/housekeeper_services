import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import {
  Injectable,
  UnauthorizedException,
  Dependencies,
} from '@nestjs/common';
import { AuthService } from './auth.service';

@Injectable()
@Dependencies(AuthService)
export class UserStrategy extends PassportStrategy(Strategy, 'user') {
  private authService: any;

  constructor(authService) {
    super({
      usernameField: 'phoneNumber',
      passwordField: 'phoneNumber',
    });
    this.authService = authService;
  }

  async validate(phoneNumber) {
    const user = await this.authService.validateUser(phoneNumber);

    if (!user) {
      throw new UnauthorizedException('incorrect username or password');
    }

    return user;
  }
}

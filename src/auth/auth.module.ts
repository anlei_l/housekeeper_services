import { forwardRef, Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './local.strategy';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './constants';
import { JwtStrategy } from './jwt.strategy';
import { UserStrategy } from './user.strategy';
import { SystemUsersModule } from '../systemUsers/systemUsers.module';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '30d' },
    }),
    forwardRef(() => SystemUsersModule),
    forwardRef(() => UsersModule),
  ],
  providers: [AuthService, UserStrategy, LocalStrategy, JwtStrategy],
  exports: [AuthService],
  controllers: [],
})
export class AuthModule {}

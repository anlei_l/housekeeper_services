import { Dependencies, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Users } from '../users/users.entity';
import { SystemUsersService } from '../systemUsers/systemUsers.service';
import { UsersService } from '../users/users.service';

@Injectable()
@Dependencies(JwtService, SystemUsersService, UsersService)
export class AuthService {
  private jwtService: JwtService;
  private systemUsersService: SystemUsersService;
  private usersService: UsersService;

  constructor(jwtService, systemUsersService, usersService) {
    this.jwtService = jwtService;
    this.systemUsersService = systemUsersService;
    this.usersService = usersService;
  }

  async systemLogin(systemUser: any) {
    const payload = { username: systemUser.username, sub: systemUser.userId };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  async getToken(user: Users) {
    const payload = { phoneNumber: user.phoneNumber, id: user.id };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  async validateSystemUser(username: string, password: string): Promise<any> {
    const user = await this.systemUsersService.findOne(username);

    if (user && user.password === password) {
      const { password, ...args } = user;
      return args;
    }

    return null;
  }

  async validateUser(user: Users): Promise<any> {
    const userInfo = await this.usersService.findOne(user);

    if (!userInfo) {
      return await this.usersService.create({ ...user });
    }

    return userInfo;
  }
}

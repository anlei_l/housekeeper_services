import {
  Bind,
  Controller,
  Post,
  UseInterceptors,
  UploadedFile,
  Request, Injectable
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { writeFile } from 'fs';
import {UploadService} from "./upload.service";

@Controller('upload')
export class UploadController {
  constructor() {}

  // @UseGuards(JwtAuthGuard)
  @Post()
  @UseInterceptors(FileInterceptor('file'))
  @Bind(UploadedFile(), Request())
  async add(file, req): Promise<any> {
    writeFile(`upload/${file.originalname}`, file.buffer, (err) => {
      if (err) {
        return console.error(err);
      }
    })
    return `http://192.168.0.245:3000/upload/${file.originalname}`
  }
}

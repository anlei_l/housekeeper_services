import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Upload } from './upload.entity';

@Injectable()
export class UploadService {
  constructor(
    @InjectRepository(Upload)
    private uploadRepository: Repository<Upload>,
  ) {}

  findAll(): Promise<Upload[]> {
    return this.uploadRepository.find({ relations: ['order'] });
  }

  async findOne(id): Promise<Upload> {
    return await this.uploadRepository.findOne(id);
  }

  async create(body: Upload): Promise<any> {
    const user = this.uploadRepository.create(body);
    const result = await this.uploadRepository.save(user);
    return result;
  }

  async remove(id: string): Promise<void> {
    this.uploadRepository.delete(id);
  }
}
